<?php
class m_all extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function get_detail_user_by_id($id) {
        $q="
            select us.*,gendre.name as gendre, vocation.name as vocation, role.name as role from user us
            left join data_list gendre on (us.gendre_id=gendre.data_list_id)
            left join ref_evidence vocation on (us.vocation_id=vocation.ref_evidence_id)
            left join data_list role on (us.role_id=role.data_list_id)
            where us.user_id = $id
        ";
        return $this->db->query($q)->result_array();	 
    }
    
    public function result_riasec_per_user($id){

        $data = array();

        $q="
            select * from answer 
            where user_id = $id
            order by answer_id desc
            limit 1        
        ";
        $header = $this->db->query($q)->result_array();

        if($header){
            $q="
            select 
            ad.answer_id,
            sum(ad.is_checked) as total,
            qg.name,
            qg.description
            from answer_detail ad
            left join question qu on (qu.question_id=ad.question_id)
            left join data_list qg on (qg.data_list_id=qu.question_group_id)
            where answer_id = ".$header[0]['answer_id']."
            group by ad.answer_id,qg.name
            ORDER BY sum(ad.is_checked) desc, qg.data_list_id  
            ";
            $detail = $this->db->query($q)->result_array();
            $data['date'] = $header[0]['date']; 
            $data['highest'] = $detail[0]['total']; 
            $data['second'] = $detail[1]['total']; 
            $data['third'] = $detail[2]['total']; 
            $data['highest_name'] = $detail[0]['name']; 
            $data['second_name'] = $detail[1]['name']; 
            $data['third_name'] = $detail[2]['name'];     
            $data['highest_desc'] = $detail[0]['description']; 
            $data['second_desc'] = $detail[1]['description']; 
            $data['third_desc'] = $detail[2]['description'];                     
        }

        return $data;


    }

    public function get_vocation_probability_by_id($id){
        $q="
            select 
            re.name as vocation, 
            rh.name as job,
            rh.ref_hypothesis_id as ref_hypothesis_id,
            coalesce(rhd.probability_value,0) as probability
            from ref_evidence re
            left join ref_hypothesis rh on (1=1)
            left join ref_hypothesis_detail rhd on (rhd.ref_evidence_id=re.ref_evidence_id and rhd.ref_hypothesis_id=rh.ref_hypothesis_id)
            where re.ref_evidence_id = $id
        ";
        return $this->db->query($q)->result_array();        
    }

    public function get_job_probability_by_id($id){
        $q="
            select
            rh.name as vocation,
            re.name as job,	
            re.ref_evidence_id as ref_evidence_id,
            coalesce(rhd.probability_value,0) as probability
            from ref_hypothesis rh
            left join ref_evidence re on (1=1)
            left join ref_hypothesis_detail rhd on (rhd.ref_evidence_id=re.ref_evidence_id and rhd.ref_hypothesis_id=rh.ref_hypothesis_id)
            where rh.ref_hypothesis_id = $id
        ";
        return $this->db->query($q)->result_array();        
    }

}
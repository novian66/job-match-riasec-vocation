<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_auth {
    public function __construct()
    {
        $this->CI =& get_instance();
    }    
    function checkLogin(){
        if($this->CI->session->userdata('user_id')==null){
            header("location: ".base_url()."login");
            die();
        }
    }
    function getInfo($nameClass){
        $user_id = $this->CI->session->userdata('user_id');
        $q="
        select mo.* from module_group mg
        left join user us on (mg.role_id=us.role_id)
        left join module mo on (mo.module_id=mg.module_id)
        where us.user_id = $user_id
        and lower(mo.value) = lower('$nameClass')
        -- and mo.is_active = 1000001
        and mg.is_active = 6
        limit 1        
        ";
        $result = $this->CI->db->query($q)->result_array();
        // print_r($result);die();
        if(empty($result)){
            header("location: ".base_url()."welcome");
            die();
        }
        else{
            $q="
            SELECT us.*,md.name role FROM `user` us
            left join data_list md on (us.role_id=md.data_list_id)
            where user_id = $user_id
            limit 1     
            ";
            $user = $this->CI->db->query($q)->result_array();

            $info['module_name'] = $result[0]['name'];
            $info['user_username'] = $user[0]['username'];
            $info['user_first_letter'] = strtoupper(substr($user[0]['name'],0,1));
            $info['user_name'] = $user[0]['name'];
            $info['user_role'] = $user[0]['role'];
            $info['user_role_id'] = $user[0]['role_id'];
            return $info;
        }
    }
}

?>
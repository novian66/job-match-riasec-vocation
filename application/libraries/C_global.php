<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_global {
    public function __construct()
    {
        $this->CI =& get_instance();
    }    
    function generate_number($table_name,$type_data=''){
        $where = "";
        if(!empty($type_data))
        $where = " and type_data='$type_data'";


        $q="
            select * from generate_number where lower(table_name) = '$table_name' $where     
        ";
        $getFormat = $this->CI->db->query($q)->result_array();
        if(!empty($getFormat)){        
            
            /* START HITUNG TOTAL */
            $q = "
                select count(created) as count from $table_name 
                where 
                1 = 1
                $where
            ";
            $getCount = $this->CI->db->query($q)->result_array();
            if($getCount)
            $count = $getCount[0]['count'];
            else
            $count = 0;
            /* END */

            /* START HITUNG TOTAL PER HARI */
            $q = "
                select count(created) as count from $table_name 
                where 
                DATE_FORMAT(created,'%Y-%m-%d') = DATE_FORMAT(now(),'%Y-%m-%d')
                $where
            ";
            $getCountPerDay = $this->CI->db->query($q)->result_array();
            if($getCountPerDay)
            $countperday = $getCountPerDay[0]['count'];
            else
            $countperday = 0;
            /* END */

            $format = $getFormat[0]['format'];
            $part = array(
                '[YY]'           => date('y'),
                '[MM]'           => date('m'),
                '[DD]'           => date('d'),
                '[COUNT]'        => $count + 1,
                '[COUNTPERDAY]'  => $countperday + 1
            );
            $result = strtr($format, $part);
            return $result;
        }
        else{
            return 'Nomor belum diatur';
        }
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
    $this->load->model('m_all');	
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['detail_user'] = $this->m_all->get_detail_user_by_id($this->session->userdata('user_id'));
    $data['result'] = $this->m_all->result_riasec_per_user($this->session->userdata('user_id'));
    $data['bayes'] = $this->proccess();
    // echo "<pre>";
    // print_r($data['bayes']);
		$this->load->view('result',$data);
  }

  public function proccess(){
    $data['detail_user'] = $this->m_all->get_detail_user_by_id($this->session->userdata('user_id'));
    $data['result'] = $this->m_all->result_riasec_per_user($this->session->userdata('user_id'));

    $data['hypothesis'] = $this->db->get_where('ref_hypothesis',array('is_active'=>6))->result_array();
    $data['conclution']['sum'] = 0;
    foreach($data['hypothesis'] as $key=>$val){
      $e1 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['detail_user'][0]['vocation_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //jurusan
      $e2 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['highest_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 1
      $e3 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['second_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 2
      $e4 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['third_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 3
      $data['hypothesis'][$key] = array(); //mengosongkan array
      $data['hypothesis'][$key]['id'] = $val['ref_hypothesis_id']; // di isi id hipotesis
      $data['hypothesis'][$key]['name'] = $val['name']; // nama hipotesis
      $data['hypothesis'][$key]['vocation'] = $data['detail_user'][0]['vocation']; //nama evidence e1
      $data['hypothesis'][$key]['value']['p'] = $val['probability_value']; // probaliti tanpa kondisi
      $data['hypothesis'][$key]['value']['e1'] = ($e1)?round($e1[0]['probability_value'],5):0; // probaliti kondisi jurusan
      $data['hypothesis'][$key]['value']['e2'] = ($e2)?round($e2[0]['probability_value'],5):0; // probaliti kondisi riasec 1
      $data['hypothesis'][$key]['value']['e3'] = ($e3)?round(($e3[0]['probability_value'])*0.75,5):0; // probaliti kondisi riasec 2 (nilai di kurangi 25% karena posisi 2)
      $data['hypothesis'][$key]['value']['e4'] = ($e4)?round(($e4[0]['probability_value'])*0.5,5):0; // probaliti kondisi riasec 3 (nilai di kurangi 50% karena posisi 3)
      $data['hypothesis'][$key]['multiplication'] = round($data['hypothesis'][$key]['value']['p']*$data['hypothesis'][$key]['value']['e1']*
      $data['hypothesis'][$key]['value']['e2']*$data['hypothesis'][$key]['value']['e3']*$data['hypothesis'][$key]['value']['e4'],5); // semua evidence di kalikan
      $data['conclution']['sum'] += $data['hypothesis'][$key]['multiplication']; // hasil perkalian evidence di tampung
    }

    //proses rata2
    foreach($data['hypothesis'] as $key=>$val){
      $data['hypothesis'][$key]['mean'] = round($data['hypothesis'][$key]['multiplication']/$data['conclution']['sum'],5);
      $vc_array_name[$key] = (float)$data['hypothesis'][$key]['mean'];
    }
    array_multisort($vc_array_name, SORT_DESC, $data['hypothesis']);
    // echo "<pre>";
    // print_r($data);
    // die();
    unset($data['detail_user']);
    unset($data['result']);
    return $data;
    }
}

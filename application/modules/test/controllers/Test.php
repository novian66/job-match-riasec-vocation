<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
  }  	
	public function index()
	{
    echo "<pre>";
    $data = $this->db->get_where('code',array())->result_array();
    foreach($data as $key=>$val){
      echo "hypothesis(".strtolower($val['name']).") :- ".strtolower($val['name']).", !.<br>";
    }


    echo "<br>";
    echo "<br>";
    echo "<br>";


    foreach($data as $key=>$val){

      $satu = substr($val['name'],0,1);
      $dua = substr($val['name'],1,1);
      $tiga = substr($val['name'],2,1);

      if($satu=="R"){
        $satu = "realistic";
      }
      elseif($satu=="I"){
        $satu = "investigative";
      }
      elseif($satu=="A"){
        $satu = "artistic";
      }
      elseif($satu=="S"){
        $satu = "social";
      }
      elseif($satu=="E"){
        $satu = "enterprising";
      }
      elseif($satu=="C"){
        $satu = "conventional";
      }               
      
      if($dua=="R"){
        $dua = "realistic";
      }
      elseif($dua=="I"){
        $dua = "investigative";
      }
      elseif($dua=="A"){
        $dua = "artistic";
      }
      elseif($dua=="S"){
        $dua = "social";
      }
      elseif($dua=="E"){
        $dua = "enterprising";
      }
      elseif($dua=="C"){
        $dua = "conventional";
      }    
      
      if($tiga=="R"){
        $tiga = "realistic";
      }
      elseif($tiga=="I"){
        $tiga = "investigative";
      }
      elseif($tiga=="A"){
        $tiga = "artistic";
      }
      elseif($tiga=="S"){
        $tiga = "social";
      }
      elseif($tiga=="E"){
        $tiga = "enterprising";
      }
      elseif($tiga=="C"){
        $tiga = "conventional";
      }          


      $pekerjaan = $this->db->get_where('code_job',array('code'=>$val['name']))->result_array();
      $pekerjaan_string = "";
      foreach($pekerjaan as $val2){
        $pekerjaan_string .= "write('".$val2['name']."'),
        nl,
        ";
      }

      if(empty($pekerjaan_string)){
        $pekerjaan_string = "write('No Match Work, sory.'),
        nl,
          ";
      }

      // print_r($pekerjaan);

      echo "
      
      ".strtolower($val['name'])." :-
        verify(".strtolower($satu)."_first),
        verify(".strtolower($dua)."_second),
        verify(".strtolower($tiga)."_third),
        write('I believe you have a job match on:'),
        nl,
        ".$pekerjaan_string."
        nl.      
      
      ";
    }    
  }

}

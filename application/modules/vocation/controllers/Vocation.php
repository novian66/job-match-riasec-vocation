<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vocation extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['vocation'] = $this->db->get_where('ref_evidence',array('is_active'=>6, 'ref_evidence_group_id'=>19))->result_array();
		$this->load->view('Vocation',$data);
  }

  public function save(){
    $insert = array(
      'value'                 => $_POST['name'],
      'name'                  => $_POST['name'],
      'ref_evidence_group_id' => 19,
      'createdby'             => $this->session->userdata('user_id')
    );
    $this->db->insert('ref_evidence', $insert);
    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Data Saved Successfully.'));
    redirect(base_url().'vocation');
  }

  public function delete(){
    $delete = array(
      'ref_evidence_id'       => $_GET['id']
    );
    $this->db->delete('ref_evidence', $delete);
    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Data Deleted Successfully.'));

    redirect(base_url().'vocation');
  }

}

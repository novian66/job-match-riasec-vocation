<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
    // $this->load->model('m_welcome');	
    // $this->load->model('m_report_p_and_l');	
  }  	
	public function index()
	{
    $data['info'] = $this->info;
		$this->load->view('welcome',$data);
	}
}

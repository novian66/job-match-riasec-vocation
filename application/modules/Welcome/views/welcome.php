
<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'template/head.php' ?>
</head>

<body>

    <div class="d-flex flex-column h-100">
    <?php include 'template/topbar.php' ?>

        <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><?php echo $info['module_name'] ?></a></li>
                    </ol>
                    <?php
                                $message = $this->session->flashdata('message');
                                $content = $this->session->flashdata('content');
                                if ($message) {
                                    echo "
                                    <div class='alert alert-".$message['condition']."' role='alert'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                        </button>                                        
                                    ".$message['text']."
                                    </div>				
                                    ";						
                                } 
                                ?>             
                </div>
            </div>
            <?php include 'template/sidebar.php' ?>
        </div>
    </div>
    <?php include 'template/footerJs.php' ?>
</body>

</html>
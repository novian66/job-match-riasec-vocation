<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maping_probability extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['job'] = $this->db->get_where('ref_hypothesis',array('is_active'=>6))->result_array();
    $data['evidence'] = $this->db->get_where('ref_evidence',array('is_active'=>6))->result_array();
    $data['detail'] = $this->db->get_where('ref_hypothesis_detail',array())->result_array();
    foreach($data['detail'] as $val){
      $data['value'][$val['ref_hypothesis_id']][$val['ref_evidence_id']]= $val['probability_value'];
    }
    // echo "<pre>";
    // print_r($data['value']);
    // // die();
    echo '
    <html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body>
    ';
    echo "<div style='overflow-x:auto;'><table>";
    echo "<tr>";
    echo "<td>#</td>";
      foreach($data['job'] as $val2){
        echo "<td>".$val2['name']."</td>";
      }
    echo "</tr>";    
    echo "<tr>";
    echo "<td>-</td>";
      foreach($data['job'] as $val2){
        echo "<td>".$val2['probability_value']."</td>";
      }
    echo "</tr>";      
    foreach($data['evidence'] as $val){
      echo "<tr>";
      echo "<td>".$val['name']."</td>";
        foreach($data['job'] as $val2){
          // echo "<td>".$val2['ref_hypothesis_id']."||".$val['ref_evidence_id']."</td>";
          $prob = (isset($data['value'][$val2['ref_hypothesis_id']][$val['ref_evidence_id']]))?$data['value'][$val2['ref_hypothesis_id']][$val['ref_evidence_id']]:'0.00000';
          echo "<td>".$prob."</td>";
        }
      echo "</tr>";
    }
    echo "</table></div>";
  }
}
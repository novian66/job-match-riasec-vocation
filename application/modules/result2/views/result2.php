
<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'template/head.php' ?>
</head>

<body>

    <div class="d-flex flex-column h-100">
    <?php include 'template/topbar.php' ?>

        <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container-fluid">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?php echo $info['module_name'] ?></a></li>
                </ol> 
                
                <?php 
                    if(empty($result)){
                        echo "</div>
                            <div class='text-center'>       
                                <h3>Sorry, you haven't tested</h3><br>
                                <a href='questionnaire'><button class='btn btn-primary'>Test Now</button></a>    
                            </div>    
                                              
                        ";
                    }
                    else{
                ?>
                <div class="text-center">       
                    <small class="text-muted-light "><?php echo $result['date'] ?></small>
                </div>
                    <div class="card-group">
                        <div class="card">
                            <div class="card-body text-center">
                                <h4 class="mb-0"><strong><?php echo $detail_user[0]['vocation'] ?></strong></h4>
                                <small class="text-muted-light">VOCATION</small>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body text-center">
                                <h4 class="text-success mb-0"><strong><?php echo $result['highest_name'].' ('.$result['highest'].')' ?></strong></h4>
                                <small class="text-muted-light">RIASEC TEST</small><br>
                                <small class="text-muted-light">Highest Total</small>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body text-center">
                                <h4 class="text-success mb-0"><strong><?php echo $result['second_name'].' ('.$result['second'].')' ?></strong></h4>
                                <small class="text-muted-light">RIASEC TEST</small><br>
                                <small class="text-muted-light">Second Highest or Tie</small>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body text-center">
                                <h4 class="text-success mb-0"><strong><?php echo $result['third_name'].' ('.$result['third'].')' ?></strong></h4>
                                <small class="text-muted-light">RIASEC TEST</small><br>
                                <small class="text-muted-light">Third Highest or Tie</small>
                            </div>
                        </div>                                                
                    </div>      
                    <div class="text-center">       
                        <h3><?php echo $result['highest_desc'] ?></h3><br>
                    </div>           

                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                        <?php echo $result['highest_name'] ?> - Job
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Job Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($highest_job as $val) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $val['name'] ?></td>
                                        </tr>
                                        <?php $no++; } ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            </div>

                        </div>
                    </div>


                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                        <?php echo $result['second_name'] ?> - Job
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Job Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($second_job as $val) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $val['name'] ?></td>
                                        </tr>
                                        <?php $no++; } ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            </div>

                        </div>
                    </div>


                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                        <?php echo $result['third_name'] ?> - Job
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Job Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($third_job as $val) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $val['name'] ?></td>
                                        </tr>
                                        <?php $no++; } ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            </div>

                        </div>
                    </div>                    
                    

                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                        [<?php echo $result['highest_name'] ?>, <?php echo $result['second_name'] ?>, <?php echo $result['third_name'] ?>] - Job
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Job Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if($combination_job){
                                            $no=1; foreach($combination_job as $val) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $val['name'] ?></td>
                                        </tr>
                                        <?php 
                                        $no++; } }
                                        else{
                                            ?>
                                            <tr><td colspan="2">
                                            <b>Sorry, Job list not available in combination [<?php echo $result['highest_name'] ?>, <?php echo $result['second_name'] ?>, <?php echo $result['third_name'] ?>]</b>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            </div>

                        </div>
                    </div>                      

                    <!-- <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            Can you?
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                            <div class="row text-center">
                                <div class="col-md-4">asdas</div>
                                <div class="col-md-4">asdas</div>
                                <div class="col-lg-4">asdas</div>
                                <div class="col-lg-4">asdas</div>
                                <div class="col-lg-4">asdas</div>
                                <div class="col-lg-4">asdas</div>
                                <div class="col-lg-4">asdas</div>
                            </div>
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>
 
                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            Like to?
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">

                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div class="card card-stats-primary">
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            Career Possibilities
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">

                            </div>
                            <div class="card-footer">
                            </div>
                        </div>

                    </div>                     -->
                </div>
                    <?php } ?>
                    
            </div>
            <?php include 'template/sidebar.php' ?>
        </div>
    </div>
    <?php include 'template/footerJs.php' ?>
</body>

</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result2 extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
    $this->load->model('m_all');	
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['detail_user'] = $this->m_all->get_detail_user_by_id($this->session->userdata('user_id'));
    $data['result'] = $this->m_all->result_riasec_per_user($this->session->userdata('user_id'));
    $data['highest_job'] = $this->db->get_where('code_job',array('holland_code'=>$data['result']['highest_code']))->result_array();
    $data['second_job'] = $this->db->get_where('code_job',array('holland_code'=>$data['result']['second_code']))->result_array();
    $data['third_job'] = $this->db->get_where('code_job',array('holland_code'=>$data['result']['third_code']))->result_array();
    $combnation = $data['result']['highest_code'].$data['result']['second_code'].$data['result']['third_code'];
    $data['combination_job'] = $this->db->get_where('code_job',array('code'=>$combnation))->result_array();
    // $data['bayes'] = $this->proccess();
    // echo "<pre>";
    // print_r($data);die();
		$this->load->view('result2',$data);
  }

  public function proccess(){
    $data['detail_user'] = $this->m_all->get_detail_user_by_id($this->session->userdata('user_id'));
    $data['result'] = $this->m_all->result_riasec_per_user($this->session->userdata('user_id'));

    $data['hypothesis'] = $this->db->get_where('ref_hypothesis',array('is_active'=>6))->result_array();
    $data['conclution']['sum'] = 0;
    foreach($data['hypothesis'] as $key=>$val){
      $e1 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['detail_user'][0]['vocation_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //jurusan
      $e2 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['highest_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 1
      $e3 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['second_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 2
      $e4 = $this->db->get_where('ref_hypothesis_detail',array('ref_evidence_id'=>$data['result']['third_id'],'ref_hypothesis_id'=>$val['ref_hypothesis_id']))->result_array(); //riasec 3
      $data['hypothesis'][$key] = array(); //mengosongkan array
      $data['hypothesis'][$key]['id'] = $val['ref_hypothesis_id']; // di isi id hipotesis
      $data['hypothesis'][$key]['name'] = $val['name']; // nama hipotesis
      $data['hypothesis'][$key]['vocation'] = $data['detail_user'][0]['vocation']; //nama evidence e1
      $data['hypothesis'][$key]['value']['p'] = $val['probability_value']; // probaliti tanpa kondisi
      $data['hypothesis'][$key]['value']['e1'] = ($e1)?round($e1[0]['probability_value'],5):0; // probaliti kondisi jurusan
      $data['hypothesis'][$key]['value']['e2'] = ($e2)?round($e2[0]['probability_value'],5):0; // probaliti kondisi riasec 1
      $data['hypothesis'][$key]['value']['e3'] = ($e3)?round(($e3[0]['probability_value'])*0.75,5):0; // probaliti kondisi riasec 2 (nilai di kurangi 25% karena posisi 2)
      $data['hypothesis'][$key]['value']['e4'] = ($e4)?round(($e4[0]['probability_value'])*0.5,5):0; // probaliti kondisi riasec 3 (nilai di kurangi 50% karena posisi 3)
      $data['hypothesis'][$key]['multiplication'] = round($data['hypothesis'][$key]['value']['p']*$data['hypothesis'][$key]['value']['e1']*
      $data['hypothesis'][$key]['value']['e2']*$data['hypothesis'][$key]['value']['e3']*$data['hypothesis'][$key]['value']['e4'],5); // semua evidence di kalikan
      $data['conclution']['sum'] += $data['hypothesis'][$key]['multiplication']; // hasil perkalian evidence di tampung
    }

    //proses rata2
    foreach($data['hypothesis'] as $key=>$val){
      $data['hypothesis'][$key]['mean'] = round($data['hypothesis'][$key]['multiplication']/$data['conclution']['sum'],5);
      $vc_array_name[$key] = (float)$data['hypothesis'][$key]['mean'];
    }
    array_multisort($vc_array_name, SORT_DESC, $data['hypothesis']);
    // echo "<pre>";
    // print_r($data);
    // die();
    unset($data['detail_user']);
    unset($data['result']);
    return $data;
    }

    public function generate_prolog(){
      date_default_timezone_set('Asia/Jakarta');
      $text = "
      /**
      * Data was generate at ".date('d-m-Y, G:i:s')."
      */

      go:-
      hypothesis(Disease),
      nl,
      nl,
      write('Good Luck! '),
      undo.      
      ";
      // echo "<pre>";
      $data = $this->db->get_where('code',array())->result_array();
      foreach($data as $key=>$val){
        $text .= "
        hypothesis(".strtolower($val['name']).") :- ".strtolower($val['name']).", !.";
      }
  
  
      echo "<br>";
      echo "<br>";
      echo "<br>";
  
  
      foreach($data as $key=>$val){
        
        $satu = substr($val['name'],0,1);
        $dua = substr($val['name'],1,1);
        $tiga = substr($val['name'],2,1);
  
        if($satu=="R"){
          $satu = "realistic";
        }
        elseif($satu=="I"){
          $satu = "investigative";
        }
        elseif($satu=="A"){
          $satu = "artistic";
        }
        elseif($satu=="S"){
          $satu = "social";
        }
        elseif($satu=="E"){
          $satu = "enterprising";
        }
        elseif($satu=="C"){
          $satu = "conventional";
        }               
        
        if($dua=="R"){
          $dua = "realistic";
        }
        elseif($dua=="I"){
          $dua = "investigative";
        }
        elseif($dua=="A"){
          $dua = "artistic";
        }
        elseif($dua=="S"){
          $dua = "social";
        }
        elseif($dua=="E"){
          $dua = "enterprising";
        }
        elseif($dua=="C"){
          $dua = "conventional";
        }    
        
        if($tiga=="R"){
          $tiga = "realistic";
        }
        elseif($tiga=="I"){
          $tiga = "investigative";
        }
        elseif($tiga=="A"){
          $tiga = "artistic";
        }
        elseif($tiga=="S"){
          $tiga = "social";
        }
        elseif($tiga=="E"){
          $tiga = "enterprising";
        }
        elseif($tiga=="C"){
          $tiga = "conventional";
        }          
  
  
        $pekerjaan = $this->db->get_where('code_job',array('code'=>$val['name']))->result_array();
        $pekerjaan_string = "";
        foreach($pekerjaan as $val2){
          $pekerjaan_string .= "write('".$val2['name']."'),
          nl,
          ";
        }
  
        if(empty($pekerjaan_string)){
          $pekerjaan_string = "write('No Match Work, sory.'),
          nl,
            ";
        }
  
        // print_r($pekerjaan);
  
        // $text .= "
        
        // ".strtolower($val['name'])." :-
        //   verify(".strtolower($satu)."_first),
        //   verify(".strtolower($dua)."_second),
        //   verify(".strtolower($tiga)."_third),
        //   write('I believe you have a job match on:'),
        //   nl,
        //   ".$pekerjaan_string."
        //   nl.      
        
        // ";

        $text .= "
        
        ".strtolower($val['name'])." :-
          write('I believe you have a job match on:'),
          nl,
          ".$pekerjaan_string."
          nl.      
        
        ";        
      }

      $text.="
      
      /* how to ask questions */
      ask(Question) :-
      write('Choose 3 from the highest, second highest, third highest:'),
      write(Question),
      write('? '),
      read(Response),
      nl,
      ( (Response == yes ; Response == y)
      ->
      assert(yes(Question)) ;
      assert(no(Question)), fail).
      
      :- dynamic yes/1,no/1.
      /*How to verify something */
      verify(S) :-
      (yes(S)
      ->
      true ;
      (no(S)
      ->
      fail ;
      ask(S))).
      /* undo all yes/no assertions*/
      undo :- retract(yes(_)),fail.
      undo :- retract(no(_)),fail.
      undo.      
      
      ";
      // echo "<pre>";
      // print_r($text);

      $myfile = fopen("C:\\xampp\\htdocs\\project_sbp\\assets\\prolog\\prolog_case.pl", "w") or die("Unable to open file!");		    
      fwrite($myfile, $text);
      fclose($myfile);



      // $output = `swipl -s prolog_case.pl -g "hypothesis(ser)."  -t halt.  `;
      // // $a = exec($output);
      // print_r($output);

      // $output = shell_exec( "C:\\xampp\\htdocs\\project_sbp\\assets\\prolog\\prolog_case.pl" );
      // echo $output;
      // echo "\n";
      // $output = `swipl -s type_personnalite.pl -g "trouver_partenaire($Nom)."  -t halt.  `;
      // exec( "swipl -s  C:\\xampp\\htdocs\\project_sbp\\assets\\prolog\\prolog_case.pl -g 'hypothesis(ser).' -t halt", $output );
      // exec(`swipl -s C:\\xampp\\htdocs\\project_sbp\\assets\\prolog\\prolog_case.pl -g "hypothesis(ser)." `, $output );

      $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Generate to Prolog Successfully.'));

      redirect(base_url().'welcome');
    
    }
}

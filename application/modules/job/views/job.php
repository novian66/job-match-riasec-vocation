
<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'template/head.php' ?>
</head>

<body>

    <div class="d-flex flex-column h-100">
    <?php include 'template/topbar.php' ?>

        <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><?php echo $info['module_name'] ?></a></li>
                    </ol>
                    <div class="card card-stats-primary">

                        <div class="card">

                        
                            <div class="card-header bg-white">


                                <?php
                                $message = $this->session->flashdata('message');
                                $content = $this->session->flashdata('content');
                                if ($message) {
                                    echo "
                                    <div class='alert alert-".$message['condition']."' role='alert'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                        </button>                                        
                                    ".$message['text']."
                                    </div>				
                                    ";						
                                } 
                                ?>  
                        

                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            Data
                                        </h4>
                                    </div>
                                    <div class="media-right">
                                        <a class="btn btn-sm btn-primary" href="#" onClick="tambahModal()">Add New</a>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="card-body">

                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Probability</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no = 1;
                                        foreach($job as $val){                                            
                                            echo "                                        
                                            <tr>
                                                <td>$no</td>
                                                <td>".$val['name']."</td>
                                                <td>".$val['probability_value']."</td>
                                                <td>
                                                    <a href='job_value?id=".$val['ref_hypothesis_id']."'><button class='btn btn-primary'>Set Value</button></a>
                                                    <a href='#' onclick='updateModal(".$val['ref_hypothesis_id'].")'><button class='btn btn-success'>Edit</button></a>
                                                    <a href='job/delete?id=".$val['ref_hypothesis_id']."'><button class='btn btn-danger'>Delete</button></a>
                                                </td>
                                            </tr>";
                                            $no++;
                                        }
                                    ?>                                    
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add New</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="job/save" method="post">
                        <div class="modal-body">                        
                            <fieldset class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Job" required>
                                <input type="hidden" class="form-control" id="ref_hypothesis_id" name="ref_hypothesis_id" placeholder="Enter Job">
                            </fieldset>          
                            <fieldset class="form-group">
                                <label for="name">Value</label>
                                <input type="decimal" class="form-control" id="value" name="value" placeholder="Enter Value" required>
                            </fieldset>                                                                  
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        </form>    
                    </div>
                </div>
            </div>            
            <?php include 'template/sidebar.php' ?>
        </div>
    </div>
    <script type="text/javascript">
    function tambahModal() {
        $("#myModal").modal('show');
        $("#myModal").find('.modal-title').text('Add New');
        $("#myModal").find('form').find('input').val('');
        $("#myModal").find('form').attr('action','job/save');
    }

    function updateModal(journal_id) {
        // alert();
        $("#myModal").modal('show');
        $("#myModal").find('.modal-title').text('Edit');
        $("#myModal").find('form').find('input').val('');
        $("#myModal").find('form').attr('action','job/edit');
        var all = JSON.stringify(<?php echo json_encode($job) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='ref_hypothesis_id']").val(obj[journal_id].ref_hypothesis_id);
        $("input[name='name']").val(obj[journal_id].name);
        $("input[name='value']").val(obj[journal_id].probability_value);
    }
</script>
    <?php include 'template/footerJs.php' ?>
</body>

</html>
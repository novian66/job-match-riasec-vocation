<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['jobs'] = $this->db->get_where('ref_hypothesis',array('is_active'=>6))->result_array();

    $data['job'] = array();
    foreach ($data['jobs'] as $key => $val) {
        $data['job'][$val['ref_hypothesis_id']] = $val;
    }

		$this->load->view('job',$data);
  }

  public function save(){
    $insert = array(
      'value'                 => $_POST['name'],
      'name'                  => $_POST['name'],
      'probability_value' => $_POST['value'],
      'createdby'             => $this->session->userdata('user_id')
    );
    $this->db->insert('ref_hypothesis', $insert);
    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Data Saved Successfully.'));
    redirect(base_url().'job');
  }

  public function edit(){
    $edit = array(
      'value'                 => $_POST['name'],
      'name'                  => $_POST['name'],
      'probability_value'     => $_POST['value'],
      'updatedby'             => $this->session->userdata('user_id')
    );
    $this->db->where('ref_hypothesis_id', $_POST['ref_hypothesis_id']);
    $this->db->update('ref_hypothesis', $edit);
    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Data Edited Successfully.'));
    redirect(base_url().'job');
  }

  public function delete(){
    $delete = array(
      'ref_hypothesis_id'       => $_GET['id']
    );
    $this->db->delete('ref_hypothesis', $delete);
    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Data Deleted Successfully.'));

    redirect(base_url().'job');
  }

}

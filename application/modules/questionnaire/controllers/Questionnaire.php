<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['question'] = $this->db->get_where('question',array('is_active'=>6))->result_array();
    $this->kshuffle($data['question']);
    // echo "<pre>";
    // print_r($data['questions']);
    // die();
		$this->load->view('questionnaire',$data);
  }
  public function process(){
    $insert = array(
      'user_id'                 => $this->session->userdata('user_id'),
      'createdby'               => $this->session->userdata('user_id')
    );
    $this->db->insert('answer', $insert);
    $answer_id = $this->db->insert_id();

    $question_input = $_POST['question'];
    $question = $this->db->get_where('question',array('is_active'=>6))->result_array();

    foreach($question as $val){
      $insert = array(
        'answer_id'                 => $answer_id,
        'question_id'               => $val['question_id'],
        'is_checked'                => (isset($question_input[$val['question_id']]))?1:0,
        'createdby'                 => $this->session->userdata('user_id')
      );
      $this->db->insert('answer_detail', $insert);      
    }
    redirect(base_url().'result');
  }

  function kshuffle(&$array) {
    if(!is_array($array) || empty($array)) {
        return false;
    }
    $tmp = array();
    foreach($array as $key => $value) {
        $tmp[] = array('k' => $key, 'v' => $value);
    }
    shuffle($tmp);
    $array = array();
    foreach($tmp as $entry) {
        $array[$entry['k']] = $entry['v'];
    }
    return true;
}
}

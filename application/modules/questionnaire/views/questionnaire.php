
<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'template/head.php' ?>
</head>

<body>

    <div class="d-flex flex-column h-100">
    <?php include 'template/topbar.php' ?>

        <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><?php echo $info['module_name'] ?></a></li>
                    </ol>
                    <div class="card card-stats-primary">

                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            Check the sentence you like
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <form action="questionnaire/process" method="post">
                            <div class="card-body">
                                <?php foreach($question as $val){ ?>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox mb-0">
                                        <input type="checkbox" name="question[<?php echo $val['question_id'] ?>]" value="1" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $val['name'] ?></span>
                                    </label>
                                </div>
                                <?php } ?>

                            </div>

                            <div class="card-footer">
                                <button class="btn btn-success float-right">
                                <i class="material-icons btn__icon--right">send</i> Submit
                                </button>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <?php include 'template/sidebar.php' ?>
        </div>
    </div>
    <?php include 'template/footerJs.php' ?>
</body>

</html>
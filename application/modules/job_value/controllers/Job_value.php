<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_value extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $this->info = $this->c_auth->getInfo(get_class($this));			
    $this->load->model('m_all');	

  }  	
	public function index()
	{
    $data['info'] = $this->info;
    $data['job_detail'] = $this->db->get_where('ref_hypothesis',array('ref_hypothesis_id'=>$_GET['id']))->result_array();
    $data['job_probability'] = $this->m_all->get_job_probability_by_id($_GET['id']);
		$this->load->view('Job_value',$data);
  }

  public function process(){
    // echo "<pre>";
    // print_r($_POST);
    // die();
    
    $data = $_POST;
    foreach($data['ref_evidence_id'] as $key=>$val){
      $delete = array(
        'ref_evidence_id'                 => $key,
        'ref_hypothesis_id'               => $_POST['ref_hypothesis_id']
      );
      $this->db->delete('ref_hypothesis_detail', $delete);

      $insert = array(
        'ref_evidence_id'                 => $key,
        'ref_hypothesis_id'               => $_POST['ref_hypothesis_id'],
        'probability_value'               => $val,
        'createdby'                       => $this->session->userdata('user_id')
      );
      $this->db->insert('ref_hypothesis_detail', $insert);
    }



    $this->session->set_flashdata('message', array('condition'=>'success','icon'=>'check','text'=>'Value saved successfully.'));

      redirect(base_url().'job');
  }

}

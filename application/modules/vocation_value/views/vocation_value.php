
<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'template/head.php' ?>
</head>

<body>

    <div class="d-flex flex-column h-100">
    <?php include 'template/topbar.php' ?>

        <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
            <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><?php echo $info['module_name'] ?></a></li>
                    </ol>
                    <div class="card card-stats-primary">

                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h4 class="card-title">
                                            <?php echo $vocation_detail[0]['name'] ?>                                            
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <form action="vocation_value/process" method="post">
                            <input type="hidden" name="ref_evidence_id" value="<?php echo $vocation_detail[0]['ref_evidence_id'] ?>" />
                            <div class="card-body">

                                <table id="datatable-example" class="table table-striped table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Probability Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no = 1;
                                        foreach($vocation_probability as $val){                                            
                                            echo "                                        
                                            <tr>
                                                <td>$no</td>
                                                <td>".$val['job']."</td>
                                                <td><input name='ref_hypothesis_id[".$val['ref_hypothesis_id']."]' value='".$val['probability']."'/></td>
                                            </tr>";
                                            $no++;
                                        }
                                    ?>                                    
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>

                            </div>

                            <div class="card-footer">
                            <button class="btn btn-success float-right">
                                <i class="material-icons btn__icon--right">send</i> Submit
                                </button>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <?php include 'template/sidebar.php' ?>
        </div>
    </div>
    <?php include 'template/footerJs.php' ?>
</body>

</html>
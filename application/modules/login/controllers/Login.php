<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
        if($this->session->userdata('user_id')!=null){
            header("location: welcome");
            die();
        }        
		$this->load->view('login');
	}

    public function check(){
        $this->load->library('encryption');
        $username=$_POST['username'];
        $password=$_POST['password'];  
        if($username==null or $password==null){
            $this->session->set_flashdata('message', array('condition'=>'warning','icon'=>'warning','text'=>'Login gagal1.'));
			$this->session->set_flashdata('content', $_POST);
            header("location: ".base_url()."login");
            die();			
        }      
        $q="
            select * from user
            where username = '$username'
            and user.is_active = '6'
            limit 1
        ";
        // echo $this->encryption->encrypt($password);die();
        $result = $this->db->query($q)->result_array();
        $password2=$this->encryption->decrypt($result[0]['password']);
        // print_r($result);die();
        if($password!=$password2){
            $this->session->set_flashdata('message', array('condition'=>'warning','icon'=>'warning','text'=>'Login gagal2.'));
			$this->session->set_flashdata('content', $_POST);
            header("location: ".base_url()."login");
            die();
        }
        if(!empty($result)){
            $this->session->set_userdata('user_id',$result[0]['user_id']);
            $this->session->set_userdata('role_id',$result[0]['role_id']);
            header("location: ".base_url()."welcome");
            die();
        }
        else{
            $this->session->set_flashdata('message', array('condition'=>'warning','icon'=>'warning','text'=>'Login gagal3.'));
			$this->session->set_flashdata('content', $_POST);
            header("location:".base_url()."login");
            die();
        }

    }

	public function logout(){
		session_destroy();
		header("location:".base_url()."login");
	}    
}

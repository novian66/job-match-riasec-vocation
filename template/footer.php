
<div class="modal fade" id="modal-stock-card">
    <div class="modal-dialog">
        <div class="modal-content">        
            <form method="post" action="stock_card">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Outlet</label>
                            </div>
                            <div class="form-group col-md-9">
                                <select class="form-control" name="outlet_id" required="">
                                    <!-- <option value=''>-- Pilih Outlet --</option>  -->
                                    <?php foreach ($info['outlet'] as $value): ?>
                                        <option value="<?=$value['outlet_id'] ?>"><?=$value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Awal</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_start" id="field-3" autocomplete="off" required="" value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Akhir</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_end" id="field-3" autocomplete="off" required=""  value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Masuk</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-list-sales-invoice">
    <div class="modal-dialog">
        <div class="modal-content">        
            <form method="get" action="list_sales_invoice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Outlet</label>
                            </div>
                            <div class="form-group col-md-9">
                                <select class="form-control" name="outlet_id" required="">
                                    <!-- <option value=''>-- Pilih Outlet --</option>  -->
                                    <?php foreach ($info['outlet'] as $value): ?>
                                        <option value="<?=$value['outlet_id'] ?>"><?=$value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Awal</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_start" id="field-3" autocomplete="off" required="" value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Akhir</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_end" id="field-3" autocomplete="off" required=""  value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Status</label>
                            </div>
                            <div class="form-group col-md-9">
                                <select class="form-control" name="sales_invoice_status_id">
                                    <option value=''>-- Pilih Semua --</option> 
                                    <?php foreach ($info['sales_invoice_status'] as $value): ?>
                                        <option value="<?=$value['master_data_id'] ?>"><?=$value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>                                    
                        </div>                        
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Masuk</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-sales-receipt">
    <div class="modal-dialog">
        <div class="modal-content">        
            <form method="GET" action="sales_receipt">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Outlet</label>
                            </div>
                            <div class="form-group col-md-9">
                                <select class="form-control" name="outlet_id" required="">
                                    <!-- <option value=''>-- Pilih Outlet --</option>  -->
                                    <?php foreach ($info['outlet'] as $value): ?>
                                        <option value="<?=$value['outlet_id'] ?>"><?=$value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Awal</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_start" id="field-3" autocomplete="off" required="" value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label for="field-3" class="control-label">Tanggal Akhir</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input class="form-control input-date-picker" type="text" name="date_end" id="field-3" autocomplete="off" required=""  value="<?=date('Y-m-d')?>"> 
                            </div>                                    
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Masuk</button>
                </div>
            </form>
        </div>
    </div>
</div>

<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">

                    <div class="footer-right">
                        <span class="footer-meta">POS Modern Tani</span>
                    </div>
                </div>


    </div>
</footer>
<div class="mdk-drawer js-mdk-drawer" id="default-drawer">
                <div class="mdk-drawer__content ">
                    <div class="sidebar sidebar-left sidebar-light sidebar-transparent-sm-up o-hidden">
                        <div class="sidebar-p-y" data-simplebar data-simplebar-force-enabled="true">
                        <ul class="sidebar-menu">
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="welcome">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">home</i>Home
                                    </a>
                                </li>
                            </ul>
                            <div class="sidebar-heading">MASTER DATA</div>
                            <ul class="sidebar-menu">
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="vocation">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons"></i> Master Vocation (Evidence)
                                    </a>
                                </li>
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="job">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons"></i> Job (Hypothesis)
                                    </a>
                                </li>
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="maping_probability" target="blank">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons"></i> Mapping Probability
                                    </a>
                                </li>  
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="generate" target="blank">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons"></i> Generate
                                    </a>
                                </li>                                                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
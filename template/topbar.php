        <!-- Navbar -->
        <nav class="navbar navbar-expand navbar-dark bg-primary m-0">

            <!-- Toggle sidebar -->
            <button class="navbar-toggler d-block" data-toggle="sidebar" type="button">
                <span class="material-icons">menu</span>
            </button>

            <!-- Brand -->
            <a href="student-dashboard.html" class="navbar-brand"><i class="material-icons">school</i> Job Match</a>

            <div class="navbar-spacer"></div>



            <!-- Menu -->
            <ul class="nav navbar-nav">
                <a class="nav-link" href="#"><?php echo  $info['user_name'].' ('.$info['user_role'].')' ?></a>
                <!-- User dropdown -->
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" data-toggle="dropdown" href="#" role="button"><img src="assets/images/people/50/guy-6.jpg" alt="Avatar" class="rounded-circle" width="40"></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="student-account-edit.html">
                        <i class="material-icons">edit</i> Edit Account
                        </a>
                        <a class="dropdown-item" href="login/logout">
                        <i class="material-icons">lock</i> Logout
                        </a>
                    </div>
                </li>
                <!-- // END User dropdown -->

            </ul>
            <!-- // END Menu -->

        </nav>
        <!-- // END Navbar -->
<div class="mdk-drawer js-mdk-drawer" id="default-drawer">
                <div class="mdk-drawer__content ">
                    <div class="sidebar sidebar-left sidebar-light sidebar-transparent-sm-up o-hidden">
                        <div class="sidebar-p-y" data-simplebar data-simplebar-force-enabled="true">
                        <ul class="sidebar-menu">
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="welcome">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">home</i>Home
                                    </a>
                                </li>
                            </ul>
                            <div class="sidebar-heading">APPLICATIONS</div>
                            <ul class="sidebar-menu">
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="questionnaire">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Questionnaire
                                    </a>
                                </li>
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="result2">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">school</i> Result
                                    </a>
                                </li>
                            </ul>
                            <div class="sidebar-heading">Data</div>
                            <ul class="sidebar-menu">
                                <!-- <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="account">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">search</i> My Account
                                    </a>
                                </li> -->
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="result2/generate_prolog">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">build</i> Generate Prolog
                                    </a>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
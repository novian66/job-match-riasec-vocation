<?php
  $message = $this->session->flashdata('message');
  $content = $this->session->flashdata('content');
  
  if ($message) {
    echo "
    <div class='card-body'>
      <div class='alert alert-".$message['condition']." alert-dismissible fade show' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
        ".$message['text']."
      </div>
    </div>";					
  } 
?>  

